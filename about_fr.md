key: about
title: A propos de nous
language: fr


### FaitMaison.co: A startup weekend dream-team


[FaitMaison.co](http://faitmaison.co) is a community where people can share their home-cooked meals with amazing people in their neighbourhood. Earning a bit of extra cash is a bonus for the chefs whilst everyone else can eat delicious homemade food at reasonable prices.
