key: terms
title: Conditions générales d'utilisation
language: fr

**Conditions générales d'utilisation**

Conditions Générales d'Utilisation du site [www.faitmaison.co], service de mise en relation entre Cuistos et Gourmands édité par Faitmaison.co Version mise en ligne le 20/01/2014.

**I – Sur les Conditions générales d'utilisation**
 
**I.1 Champ d'application et définitions**
 
Les présentes conditions générales s'appliquent aux services proposés par le site [www.faitmaison.co]
Le service de mise en relation proposé sur ce site web est édité par Faitmaison.co – Société en cours de création

Les serveurs du site [www.faitmaison.co] sont hébergés aux États-Unis par la société NodeJitsu ayant son siège social à 135 E 27th Street, New York, NY, 10016 
https://www.nodejitsu.com/company/contact

 Dans les présentes,
 
« Faitmaison » : désigne la société éditant le service de mise en relation entre Cuistos et Gourmands

« Site » : désigne sans distinction le www.faitmaison.co et plus généralement tout site internet édité ou co-édité par Faitmaison ou encore tout site reprenant avec l'autorisation de Faitmaison les informations publiées sur les sites qui précèdent et proposant des services similaires.

« Cuisto » : désigne la personne physique proposant sur le site de cuisiner pour une autre personne physique pour une somme d'argent préalablement convenue, et pour un plat, un nombre de part et un horaire qu'elle a définis.

« Gourmand » : désigne la personne physique ayant accepté la proposition de plat
« Plat » : désigne une opération de service pour laquelle le Cuisto a accepté de préparer un plat pour le gourmand pour une somme d'argent déterminée à l'avance et pour un plat, un nombre de part et un horaire définis.

« Participation aux Frais » : Somme d'argent demandée par le Cuisto et acceptée par le Gourmand pour le nombre de part du plat au titre de sa participation aux frais de cuisine.Cette participation doit être strictement limitée au partage des frais et le cuisto ne peut en aucun cas tirer un quelconque bénéfice de cette opération. A défaut, le cuisto supportera seul les risques de requalification de sa prestation non autorisée par l’intermédiaire de Faitmaison.

« Membre » : désigne indistinctement le Gourmand ou le Cuisto ayant accepté les présentes Conditions Générales d'Utilisation et utilisant le site en tant que Gourmand ou Cuisto. Les Membres sont seuls décisionnaires de la concrétisation du service de préparation de plat.

« Compte Utilisateur » : désigne le compte qui doit être créé pour pouvoir devenir Membre et accéder aux services proposés par le Site.

« CGU » : désigne les présentes Conditions Générales d'Utilisation, comprenant la Charte de bonne conduite ci-après.

« Service » : désigne tout service rendu au moyen du Site à un Membre, étant précisé que Faitmaison n'est jamais partie à un contrat de préparation de plat.
 
**I.2 Acceptation en ligne des Conditions Générales d'Utilisation**

L'utilisation du Site est subordonnée à l'acceptation des présentes CGU. Au moment de la création du Compte Utilisateur, les Membres doivent cliquer la case « J'accepte les Conditions Générales d'Utilisation du site et du service proposé ».

Seule l'acceptation de ces CGU permet aux Membres d'accéder aux services proposés par le Site. L'acceptation des présentes CGU est entière et forme un tout indivisible, et les Membres ne peuvent choisir de voir appliquer une partie des CGU seulement ou encore formuler des réserves.

En acceptant les CGU, le Membre accepte notamment la Charte de bonne conduite ainsi que l'article VII des CGU concernant le « Traitement des données personnelles des utilisateurs ».

En cas de manquement à l'une des obligations prévues par les présentes, Faitmaison se réserve la possibilité de supprimer le Compte Utilisateur concerné. Il en est en particulier ainsi dès qu'un Cuisto intervient à titre professionnel.
 
**I.3 Modification des Conditions Générales d'Utilisation**
 
Faitmaison se réserve le droit de modifier à tout moment les CGU, les fonctionnalités offertes sur le site ou les règles de fonctionnement du Service.

La modification prendra effet immédiatement dès la mise en ligne des CGU que tout utilisateur reconnait avoir préalablement consultées.
Lorsque la modification survient après le paiement par le Gourmand d'une somme d'argent correspondant à la Participation aux Frais, la modification ne s'applique pas à la transaction en cours.
Faitmaison se réserve notamment le droit de proposer des services nouveaux, gratuits ou payants sur le Site.

**II- Utilisation du Service**
 
**II.1 Compte Utilisateur**
 
Pour s'inscrire et bénéficier du service proposé en ligne par Faitmaison, chaque Membre doit au préalable créer un Compte Utilisateur, en fournissant des données personnelles le concernant, indispensables au bon fonctionnement du service de mise en relation de personnes (notamment nom, prénom, âge, civilité, numéro de téléphone et adresse e-mail valides). Les Membres certifient être âgés de plus de 18 ans au moment de leur inscription.
Faitmaison ne pourra en aucun cas être tenue responsable des informations qui pourraient être erronées ou frauduleuses communiquées par les Membres.
Le Membre s'engage à ne pas créer ou utiliser d'autres comptes que celui initialement créé, que ce soit sous sa propre identité ou celle de tiers. Toute dérogation à cette règle devra faire l'objet d'une demande explicite de la part du Membre et d'une autorisation expresse et spécifique de Faitmaison. Le fait de créer ou utiliser de nouveaux comptes sous sa propre identité ou celle de tiers sans avoir demandé et obtenu l'autorisation de Faitmaison pourra entraîner la suspension immédiate des comptes du Membre et de tous les services associés.
 
**II.2 Utilisation du Service à titre non professionnel et non commercial**
 
Les Membres s'engagent à n'utiliser le service que pour la mise en relation, à titre non professionnel et non commercial, de personnes souhaitant obtenir un plat préparé par des utilisateurs du site.
Faitmaison ne pourra en aucun cas être tenue responsable d'une utilisation à titre professionnel ou commercial des services proposés par le Site.
Pourra être considérée comme activité professionnelle toute activité sur le site qui, par la nature des plats proposés, leur fréquence ou le nombre de Gourmands transportés, entrainerait une situation de bénéfice pour le Cuisto. La Participation aux Frais demandée par le Cuisto à ses Gourmands ne doit en effet rester qu'une participation aux frais, et le Cuisto ne doit en aucun cas réaliser de bénéfice. Ainsi, le Cuisto s'engage à effectuer le calcul de tous ses frais (ingrédients, carburants pour obtenir les ingrédients, électricité, gaz, eau, dépréciation de la cuisine et tout autre frais rentrant en compte) et à s'assurer que le montant demandé à ses Gourmands ne lui fait réaliser aucun bénéfice.
L’activité de Faitmaison se limitant à une mise en relation de personnes proposant un nombre de part de plat entre particuliers à titre non professionnel, les Cuistos ne peuvent être considérés comme exerçant une activité de cuisine. Le site Faitmaison.co  constituent en effet une simple plateforme, et à aucun moment la société Faitmaison n’interfère dans la préparation des plats effectués, la remise des plats, les horaires. Le Cuisto ne peut s’engager  à assurer les obligations qui sont celles d’un restaurateur et le Gourmand ne peut s’attendre à l’exécution de telles prestations.
Toute préparation de plat effectuée doit préalablement faire l’objet d’un accord préalable sur le site www.faitmaison.co . Le Gourmand doit se rendre à l’endroit convenu, et ne peut stationner sur la voie publique dans l’attente d’un Cuisto sans avoir préalablement convenu d’un rendez-vous avec ce dernier.

Faitmaison se réserve le droit d'exclure sans préavis tout Membre qui méconnaitrait notamment les présentes dispositions.
 
**II.3 Le Service sans Réservation**

Faitmaison propose un service gratuit par lequel le Gourmand contacte le Cuisto pour fixer un rendez-vous et les éventuelles détails du plat (ingrédients, taille des parts, horaires,...). Le Gourmand et le Cuisto assument alors entièrement les risques concernant notamment les annulations de dernière minute, le changement à la dernière minute ou le non-paiement de la Participation aux Frais.

Dans ce type de transaction, en aucun cas la responsabilité de Faitmaison ne saurait être engagée en cas de survenance notamment de l'un de ces risques.

L’éligibilité d'un plat ou d'un membre au service "avec Réservation" ou "sans Réservation" reste à la seule discrétion de Faitmaison et n'est donc pas au choix du membre.
 
**II.4 Le Service avec Réservation**
 
Faitmaison propose à ses Membres un service de Réservation qui permet notamment de réduire les risques d'une annulation de dernière minute de la part d'un Gourmand, ou encore le non-paiement du Plat par celui-ci.
Cependant, en aucun cas la responsabilité de Faitmaison ne pourra être engagée en raison notamment de la survenance de l'un de ces risques. La formule « Service avec Réservation » ouvre droit au Cuisto à une indemnisation en cas d'annulation tardive d'un Gourmand dans les limites déterminées ci-après.
La formule « Service avec Réservation » permet par ailleurs au Gourmand de s'assurer que la Participation aux Frais ne sera encaissée par le Cuisto que lorsque la prestation a bien été effectuée.
 
**II.4.1 Conditions dans lesquelles les Membres peuvent bénéficier de ce service**
 
La formule « Service avec Réservation » est limitée aux Membres et Plats définis par le présent article. Le fait de bénéficier d'un Compte Utilisateur sur le Site ne suffit cependant pas à accéder à la formule « Service avec Réservation ».
Seuls les plats respectant les conditions cumulatives énoncées ci-dessous peuvent être proposés dans le cadre de la formule « Service avec Réservation » :
Les Plats pour lesquels une annonce a été rédigée précisant l’horaire, le lieu, les ingrédients
Et
Les Plats dont le montant est supérieur ou égal à 5 Euros (cinq euros)

Sans préjudice de ce qui précède, Faitmaison se réserve expressément et discrétionnairement le droit de limiter le nombre et/ou la catégorie des Membres pouvant bénéficier de ce service de réservation.
 
**II.4.2 Réservation et Confirmation de Réservation**
 
Le Cuisto propose les Plats sur le Site en précisant les ingrédients, le lieu et la date de disponibilité du plat, ainsi que toute autre information qu’il jugerait nécessaire.
Le Gourmand réserve une ou plusieurs parts à partir du site en effectuant exclusivement un paiement en ligne au moyen d'une carte bancaire. La réception du paiement par Faitmaison constitue la confirmation de Réservation (ci-après la « Confirmation de Réservation »). Passé la Confirmation de Réservation, le Cuisto et le Gourmand sont irrévocablement engagés en particulier par l'application des « Dispositions financières » ci-après.
Le Gourmand et le Cuisto sont informés que la transaction est irrévocablement conclue, par un email envoyé au Gourmand lui confirmant la Réservation, et un email envoyé au Cuisto l'informant de la Réservation. Toute annulation postérieure à cette Confirmation de Réservation est encadrée par les modalités prévues par la Section « Paiement et frais en cas d'annulation » des présentes. La Réservation est nominative. Le Cuisto comme le Gourmand doivent correspondre à l'identité communiquée à Faitmaison. Le Cuisto comme le Gourmand sont en droit de considérer que l'annulation est imputable à celui dont l'identité correspond à celle mentionnée sur le site.

Le Cuisto s'engage envers les Gourmands à préparer le Plat qu'il a proposé en Réservation et qu'il a accepté de partager avec eux, aux date, heure, et lieu convenus. Il s'engage également à fournir le nombre de parts effectivement réservées par chaque Gourmand.
 
**II.4.3 Paiement et frais en cas de déroulement normal de la mise en relation**
 
*Frais de Service*

Dans le cadre des Plats avec Réservation, Faitmaison prélève des frais de service (ci-après « les Frais de Service ») sur le Site. Ces frais sont ajoutés à la transaction à la date du du paiement en ligne par le Gourmand. Les Frais de Service sont constitués d'un montant fixe et d’une part variable, auxquels vient s’ajouter le montant de la TVA en vigueur (20%). Les Frais de Service sont également dépendants du moment où est enregistrée la réservation et calculés comme suit:
-(0,55€HT + 6,6%HT du reversement Cuisto par part) * (1 + TVA 20%) ce qui correspond à 0.66€ TTC fixes + 7.92% TTC du reversement Cuisto par part.

Les Frais de Service s'appliquent pour chaque part proposée Cuisto et proposée à la Réservation.

*Paiement du Cuisto*

Le Gourmand dispose d'un délai de 7 jours (sept jours calendaires) pour confirmer expressément à Faitmaison que le plat a été servi. A l'issue de ce délai, et en l'absence de confirmation ou de contestation par le Gourmand, Faitmaison considère que la confirmation par le Gourmand est implicite.
A compter de cette confirmation expresse ou tacite, le Cuisto dispose d'un crédit exigible sur son Compte Utilisateur. Ce crédit correspond au montant payé par le Gourmand diminué des Frais de Service.
Lorsque le Plat est validé par le Gourmand, le Cuisto peut donner une instruction afin d'être payé. Faitmaison transmet les ordres de paiement le premier jour ouvré suivant la demande faite sur le site par le Membre ou à défaut automatiquement un mois après la mise disposition sur son profil des sommes concernées.
A cet effet, le Cuisto communique à Faitmaison les informations bancaires présentes sur son Relevé d'Identité Bancaire ou Relevé d'Identité Postale, permettant le virement sur son compte. Cette information est exclusivement à renseigner par le membre sur la page « Mon profil » > « Mes coordonnées bancaires »

En aucun cas, Faitmaison ne versera la somme due autrement que par virement bancaire, excluant de fait tout paiement en numéraire ou par chèque.
Faitmaison n'est en aucun cas responsable ou garant à l'égard du Cuisto d'un incident de paiement si, pour quelque cause que ce soit, la somme payée par le Gourmand venait à devoir être reversée notamment en cas d'opposition sur la carte ou d'utilisation frauduleuse. Le Cuisto s'oblige à restituer à Faitmaison à première demande toute somme reçue remise en cause du fait d'un tel incident de paiement.

*Obligations du Cuisto*

Il résulte notamment des présentes que le Cuisto a pour obligation :
De se présenter à l'heure et au lieu convenu avec le Plat et le nombre de part convenu; à défaut ou en cas d'annulation, Faitmaison se réserve le droit de garder ces informations d'annulation en base de données sur son profil et/ou de publier cette information sur son profil en ligne et/ou de suspendre l'accès au Site au Cuisto.
D'informer sans délai les Gourmands de toute modification du Plat
Si un ou plusieurs Gourmands ont réservé et que le Cuisto décide de changer une quelconque condition du Plat avec Réservation telle qu'initialement renseignée, le Cuisto s'engage à entrer en contact au plus vite avec ses Gourmands ayant effectué une Réservation pour son Plat et à obtenir l'accord des Gourmands sur ce changement. Si un Gourmand refuse ce changement, alors il est en droit d'annuler complètement sa Réservation sans qu'aucun frais d'annulation ne lui soit facturé, et sans qu'aucune indemnisation ne soit versée au Cuisto.
Attendre le Gourmand sur le lieu du rendez-vous jusqu'à 30 minutes au-delà de l'heure convenue (tolérance ne dispensant pas le Gourmand d'être ponctuel)

**II.4.4 Paiement et frais en cas d'annulation**
 
L'annulation par le Cuisto ou le Gourmand postérieure à la Confirmation de Réservation est soumise aux dispositions ci-après. En cas d'annulation imputable au Cuisto, le Gourmand est remboursé de la totalité de la somme qu'il a versée.

En cas d'annulation imputable au Gourmand :

Si le Gourmand annule plus de 24 heures avant l'heure prévue pour le départ, des frais d'annulation correspondant au montant des frais de service payés lors de l'enregistrement de la réservation sont dus à Faitmaison. En conséquence, le Gourmand sera remboursé de la somme payée diminuée du montant de ces frais d'annulation.

Si le Gourmand annule moins de 24 heures ou 24 heures avant l'heure prévue pour le rendez-vous : 
des frais d'annulation correspondant au montant des frais de service payés lors de l'enregistrement de la réservation sont dus à Faitmaison et le Cuisto perçoit un dédommagement de 50% du montant du reversement Cuisto. En conséquence, le Gourmand sera remboursé du solde soit la somme payée diminuée du montant des frais d'annulation et du dédommagement du Cuisto.

Si le Gourmand annule après l'heure prévue pour le rendez-vous ou bien s'il ne se présente pas au lieu de rendez-vous au plus tard dans un délai de 30 minutes à compter de l'heure convenue : le Cuisto perçoit un dédommagement de 100% du reversement Cuisto et les frais de service sont dus à Faitmaison. En conséquence, le Gourmand ne percevra aucun remboursement.

Lorsque l'annulation intervient avant l’heure de rendez-vous prévue et du fait du Gourmand, la ou les parts annulé(e)s par le Gourmand sont de plein droit remises à la disposition d'autres Gourmands pouvant les réserver en ligne et en conséquence soumises aux conditions des présentes.
 
**II.5 Dispositions financières dans le cadre des Trajets avec Réservation**
 
Les sommes reçues par Faitmaison sont déposées sur un compte de cantonnement ouvert dans les livres de la banque XXX. Les sommes ainsi déposées sont affectées au paiement des Cuistos. Les ordres de paiement donnés par les Membres conformément aux présentes sont irrévocables et seront exécutés par la banque XXX.

De convention expresse il est convenu par le Gourmand et le Cuisto que les sommes reçues n'emportent pas le droit à des intérêts. Le Gourmand et le Cuisto acceptent de répondre à toute demande de Faitmaison et/ou de la BCME et plus généralement de toute autorité administrative ou judiciaire compétente en relation avec la prévention ou la lutte contre le blanchiment et, en particulier, ils acceptent de fournir tout justificatif d'adresse ou d'identité utile. En l'absence de réponse immédiate à ces demandes, le Gourmand et le Cuisto acceptent par avance que Faitmaison prenne unilatéralement toute mesure qui lui semblera appropriée notamment le gel des sommes versées et/ou la suspension des services utilisés par le Gourmand ou offerts par le Cuisto.

Aucun paiement ne pourra intervenir en faveur du Cuisto sur un compte qui n'aura pas été ouvert à son nom.
 
**II.6 Assurance**
 
Le Cuisto s'engage à être assuré conformément à la législation en vigueur et à vérifier préalablement à la préparation du plat la pleine validité de son assurance. A titre d'information uniquement, il est habituellement considéré qu'un Gourmand participant aux frais de cuisine est considéré comme nourri à titre gratuit, est un tiers, et est donc couvert par son assurance responsabilité civile


Le Cuisto et le Gourmand sont informés du fait que les assurances peuvent refuser de couvrir les dommages pouvant survenir lors du Service à l'occasion duquel le Cuisto aurait réalisé un bénéfice ou aurait été dans une situation assimilée à une activité professionnelle telle que décrite à l’article II.2 des présentes.

Il supporterait seul les conséquences financières résultant de l'absence de prise en charge d'un éventuel accident par son assurance, sans que la responsabilité de Faitmaison ne puisse être engagée.

Faitmaison se réserve le droit de suspendre immédiatement le compte du Membre et les sommes y figurant et de porter à la connaissance des autorités compétentes toute activité à caractère professionnel, telle que décrite à l'article II.2 des présentes.
 
**II.7 Gestion des litiges entre Membres**
 
Faitmaison met à la disposition de ses Membres un service en ligne de règlement des litiges. Ce service est notamment destiné à régler les contestations relatives aux annulations des Réservations.
Le service de règlement des litiges n'a pas d'obligation de résultat quant à trouver une solution aux litiges entre ses Membres.
Si aucune solution n'est trouvée au litige opposant le Gourmand au Cuisto, Faitmaison se réserve le droit de retenir les sommes payées par le Gourmand jusqu'à un accord amiable entre le Gourmand et le Cuisto ou une décision judiciaire définitive.
 
**II.8 Certification du numéro**
 
Afin d'augmenter son capital confiance, d'éviter les fautes de frappe et les numéros obsolètes, chaque Membre doit certifier son numéro de téléphone portable.
A cette fin le Membre reçoit, lorsqu'il renseigne son numéro sur le site, un SMS avec un code à 4 chiffres qu'il doit renseigner ensuite sur son profil sur la page concernée.
Seuls les numéros de portables ont la possibilité d'être certifiés.
Ce service est gratuit, sauf le coût d'envoi d'un SMS dont le coût dépend de l'opérateur de téléphonie du Membre.
 
 
**III- Responsabilité**
 
La préparation de plat pour un tiers résultant exclusivement de l'accord intervenu entre le Cuisto et les Gourmands, les utilisateurs du service (Cuistos comme Gourmands) agissent sous leur seule et entière responsabilité. A ce titre, la préparation effective du plat proposé par le Cuisto et accepté par le Gourmand ne saurait entrainer de responsabilité imputable à Faitmaison, sur quelque fondement que ce soit, le service proposé par Faitmaison étant un service d'intermédiation.
La responsabilité de Faitmaison ne peut notamment pas être engagée en raison d'un sinistre qui serait survenu pour des raisons telles que :

la communication par le Cuisto d'informations erronées concernant le Plat et ses modalités ;

l'annulation du Plat par le Cuisto ou le Gourmand ;

le versement de la Participation aux Frais dans le cas d'une préparation sans réservation ;

le comportement frauduleux ou la faute du Cuisto ou du Gourmand pendant, avant, ou après la Préparation du Plat et sa consommation.

En particulier, aucune forme de responsabilité ne pourra être retenue à l'égard de Faitmaison en cas d'utilisation frauduleuse d'instruments de paiement par le Gourmand. Dans ce cas, aucune garantie de paiement du Cuisto n'est assurée par Faitmaison.

Faitmaison s'efforce d'assurer la disponibilité du site et du service 24 heures sur 24, et 7 jours sur 7. Cependant, il peut arriver que l'accès au site ou au service soit interrompu dans le cadre d'opérations de maintenance, de mises à niveau matérielle ou logicielle, de réparations d'urgence du site, ou par suite de circonstances indépendantes de la volonté de Faitmaison (comme par exemple, défaillance des liaisons et équipements de télécommunications). Faitmaison s'engage à prendre toutes les mesures raisonnables pour limiter ces perturbations, pour autant qu'elles lui soient imputables.

Les Membres reconnaissent et acceptent que Faitmaison n'assume envers eux aucune responsabilité pour toute indisponibilité, suspension ou interruption du site ou du service et ne peut être tenue responsable des préjudices directs et indirects de toute nature résultant de ce fait.

Dans tous les cas, et sans préjudice de ce qui vient d'être exposé ci-dessus et dans les autres clauses concernant l'absence de responsabilité de Faitmaison, toute cause de responsabilité qui pourrait être retenue à l'encontre de Faitmaison ne pourra donner lieu qu'au versement de dommages et intérêts dont le montant sera limité aux montants encaissés au titre des Frais de Service ou des frais d'annulation.

Les Cuistos sont seuls responsables du prix fixé pour leur annonce de préparation de Plat.
 
**IV- Interruption et suspension d'accès au service et/ou au site**
 
En cas de non respect de votre part de tout ou partie des CGU, vous reconnaissez et acceptez que Faitmaison peut à tout moment, sans notification préalable, interrompre ou suspendre, de manière temporaire ou définitive, tout ou partie du Service ou votre accès au site (y compris notamment votre Compte Utilisateur) en cas de non-respect des CGU ou pour toute raison objective.
 
**VII- Traitement des données personnelles des utilisateurs et contenu du site**
 
**VII.1 Dispositions générales**
 
Conformément aux dispositions de la loi n°78-17 du 6 janvier 1978 modifiée par la loi n°2004-801 du 6 août 2004 relative à l'informatique, aux fichiers et aux libertés, Faitmaison va procéder dès son enregistrement effectif au Tribunal du Commerce auprès de la Commission Nationale de l'Informatique et des Libertés (CNIL) à la déclaration préalable du traitement qu'elle opère sur les données personnelles vous concernant, conformément à la norme simplifiée n°48 (délibération CNIL n°2005-112 du 7 juin 2005 portant création d'une norme simplifiée concernant les traitements automatisés de données à caractère personnel relatifs à la gestion des fichiers de clients et de prospects – JO n°149 du 28 juin 2005).

Le responsable du traitement des données vous concernant est Faitmaison. Conformément aux dispositions de la norme simplifiée n°48, ont seuls accès à vos données personnelles les salariés et prestataires de service de Faitmaison, dont la tâche consiste à faire fonctionner le site ou à rendre le service.
Les données indiquées comme obligatoires dans le formulaire permettant de devenir utilisateur du site et du service nécessitent une réponse exacte de votre part. Tout défaut de réponse ou toute réponse jugée anormale par Faitmaison est susceptible d'entrainer le refus de Faitmaison de prendre en compte votre demande d'inscription au service.
Les données collectées par Faitmaison dans le cadre des services rendus sur le Site sont traitées conformément aux dispositions de la norme simplifiée n°48 et de la loi « Informatique et Libertés » du 6 août 2004.
 
**VII.2 Transmission de vos données personnelles à des tiers**
 
Conformément aux dispositions de la norme simplifiée n°48, Faitmaison se réserve le droit de transmettre tout ou partie des données personnelles concernant les Membres dans le strict respect de la norme simplifiée n°48 et de la loi « Informatique et Libertés » du 6 Août 2004.
Tout usage de données personnelles autre que celui indiqué ci-dessus devra faire l'objet d'un consentement individuel, préalable et explicite de la part des Membres.
Conformément à la loi n°78-17 du 6 janvier 1978, Faitmaison recueillera l'accord des Membres pour toute transmission de ses données à des partenaires commerciaux pour des opérations de marketing direct au moyen d'une case à cocher.
 
**VII.3 Droit d'accès, de rectification et d'opposition**
 
Conformément à la loi n°78-17 du 6 janvier 1978, vous disposez d'un droit d'accès et de rectification sur les données personnelles vous concernant, en vous adressant au webmaster du Site, sauf pour les données personnelles que vous avez données à Faitmaison lorsque vous avez rempli votre formulaire d'adhésion en ligne et qu'il vous appartient seul de modifier et de mettre à jour, ainsi qu'il est dit à l'article VII.4 « Mise à jour des données personnelles ».

Conformément à la loi n°78-17 du 6 janvier 1978, vous disposez du droit de vous opposer sans frais et gratuitement, à ce que les données personnelles vous concernant soient utilisées à des fins de prospection, notamment commerciale, par Faitmaison ou par ses partenaires commerciaux. Si votre droit d'opposition est exercé pour ce motif directement auprès de Faitmaison, cette dernière s'engage à répercuter votre opposition auprès de ses partenaires contractuels auxquels elle aurait éventuellement transmis vos données personnelles.

Conformément à la loi n°78-17 du 6 janvier 1978, Faitmaison s'engage à ce que les données personnelles vous concernant, même celles transmises à d'éventuels partenaires commerciaux, ne soient pas transmises hors de l'Union Européenne, sauf à recueillir au préalable votre consentement exprès en ce sens.

Conformément à la loi n°78-17 du 6 janvier 1978, Faitmaison se réserve le droit de transmettre les données personnelles vous concernant, soit pour respecter une obligation légale, soit en application d'une décision judiciaire, administrative, ou d'une autorité administrative indépendante (comme par exemple la Commission Nationale de l'Informatique et des Libertés).
 
**VII.4 Mise à jour des données personnelles**

Vous vous engagez à assurer, en tant que de besoin, la mise à jour des données personnelles vous concernant. Vous vous engagez en outre à ce que les données personnelles vous concernant soient exactes, complètes et non équivoques. Vous pouvez à tout moment accéder, en utilisant sur le site votre mot de passe et votre login, à votre Compte Utilisateur contenant l'ensemble des données personnelles que vous avez fournies à Faitmaison.
 
**VII.5 Sécurité des données personnelles**

Conformément à la loi n°78-17 du 6 janvier 1978, Faitmaison s'engage à prendre toute précaution utile, au regard de la nature des données et des risques présentés par le traitement, pour préserver la sécurité des données personnelles vous concernant et, notamment, empêcher que vos données personnelles ne soient déformées, endommagées, ou que des tiers non autorisés y aient accès.
 
**VII.7 Hébergement du site**

L'hébergeur du site mentionné à l'article 1.1 «Champ d'application et définitions» agit en qualité de sous-traitant de Faitmaison, au sens de la loi n°78-17 du 6 janvier 1978, seulement sur instruction écrite de Faitmaison, et ne dispose pas du droit d'utiliser les données personnelles des utilisateurs auxquelles il est susceptible d'avoir accès, sauf aux fins d'exécution des prestations techniques d'hébergement et de gestion des bases de données et seulement dans les conditions contractuelles signées entre l'hébergeur et Faitmaison qui ne peuvent déroger au présent article.
 
**VII.8 Propriété intellectuelle**

Le site www.faitmaison.co est la propriété de Faitmaison. Toute reproduction même partielle est subordonnée à l'autorisation préalable et écrite de Faitmaison. Tout lien hypertexte dirigé vers une autre page que la page d'accueil du site est soumis à l'autorisation préalable et écrite de Faitmaison.
Le visiteur du Site s'engage à ne reproduire aucun des éléments du Site. Toute utilisation contraire de tout ou partie du Site ou de l'un quelconque de ses éléments constituerait une contrefaçon susceptible d'entraîner des poursuites civiles et/ou pénales et d'exposer le visiteur contrevenant aux peines rappelées.
 
**VII.9 Contenu du site saisi par les Membres.**

Faitmaison n'est pas responsable du contenu saisi sur le Site par les Membres. Il en est particulièrement ainsi pour l'« Agenda » reprenant des informations relatives à des évènements publics ou privés, informations renseignées par des Membres. Cependant dès qu'il aura été porté à la connaissance de Faitmaison qu'un contenu porterait atteinte aux droits de tiers, Faitmaison fera tout son possible pour supprimer sans délai du Site le contenu litigieux.
 
**VII.10 Sites partenaires**

En acceptant les présentes CGU, vous acceptez que les informations fournies au moment de l'inscription soient publiées sur les sites de Faitmaison.
Faitmaison se réserve le droit de reproduire toute information figurant sur le Site ou sur des sites partenaires.
En particulier, les annonces publiées sur l'un des sites édités ou co-édités par Faitmaison peuvent être reproduites sur d'autres sites édités ou co-édités par Faitmaison ou des tiers.
 
**VIII- Litiges et prescriptions**
 
**VIII.1 Prescription**

Toute réclamation en relation contre la Société avec les présentes se prescrit dans un délai d'un an. Il est irréfragablement présumé que le Cuisto ou le Gourmand renonce à un paiement qui n'aura pas été demandé dans un délai d'un an. Les sommes non réclamées sont acquises à Faitmaison.
 
**VIII.2 Juridictions compétentes**
En tant que de besoin, la juridiction compétente en cas de litige sera considérée comme étant les tribunaux compétents du lieu du siège de la Société.
 
**CHARTE DE BONNE CONDUITE**
 
**1 Charte de bonne conduite**

Les dispositions du présent article forment ensemble la « Charte de bonne conduite de la Préparation de plat». Cette Charte est l'esprit même du service de préparation de plat proposé par Faitmaison dont chaque utilisateur est solidaire, responsable et respectueux. Elle s'applique à tous les utilisateurs du site et du service proposé par Faitmaison.

**2 Préparation de plats**

Le Cuisto s'engage à ne publier sur le site que des Plats correspondant à une préparation réellement envisagées. Les informations sur la Participation aux Frais, les ingrédients, la recette, l’horaire de disponibilité et le lieu de rendez-vous propres au Plat ou à l'auteur de l'annonce sont fournis directement par les utilisateurs et n'engagent pas Faitmaison de quelque manière que ce soit. Le Cuisto s'engage à préparer le plat qu'il a publié et à voyager avec les Gourmands avec lesquels il s'est engagé.

**3 Sécurité**

Chaque Cuisto s'engage à ne pas prendre de risque d’hygièene et à ne pas avoir quelque comportement pouvant menacer la sécurité et la santé du Gourmand.

**4 Transparence**

Un Membre doit être en mesure de présenter immédiatement ses papiers à un autre Membre dans le cadre de la préparation de Plat si celui-ci les lui demande. Les identités du Gourmand et du Cuisto doivent correspondre à celles communiquées à Faitmaison. Il est par ailleurs strictement interdit d'utiliser l'intermédiation du Site à des fins professionnelles.

**5 Frais**

Le ou les Gourmand(s) participe(nt) aux frais selon le montant indiqué sur l'annonce du Cuisto.

**6 Ponctualité**

Chacun est tenu de respecter les horaires fixés pour le rendez-vous. Le Cuisto et le Gourmand doivent se présenter au lieu et à l'heure convenue pour le rendez-vous. Au-delà d'une tolérance de 30 minutes après l'heure convenue, celui qui n'est pas présent est responsable de l'annulation.

**7 Loi**
Le Cuisto est en règle avec la loi. Par ailleurs, les deux parties s'engagent à ne transporter aucune substance, ni aucun objet illicite ou dangereux.

**8 Propreté**

Chaque Membre est tenu d'être propre. Le Cuisto portera une attention particulière à l’hygiène, s’engage à se laver les mains toutes les quinzes minutes ou dès qu’il quitte son activité de cuisine ou a un comportement pouvant menacer de quelques façons que ce soit la santé du Gourmand. 

**9 Confort et conditions**

De manière générale, chacune des conditions suivantes doit faire l'objet d'un accord entre le Cuisto et son ou ses Gourmands, préalablement à la préparation du plat: Participation aux Frais, horaires, lieu de rendez-vous.

**10 Informations publiées sur le site**

Aucun utilisateur du site ne peut y publier des informations diffamantes, injurieuses ou portant atteinte à des tiers. Faitmaison supprimera dès en avoir pris connaissance toute information contraire à son éthique. Cuistos et Gourmands acceptent par avance que les avis les concernant puissent être publiés sur le Site. Ils acceptent par ailleurs que leur niveau d'expérience soit calculé et publié en fonction des critères décidés par Faitmaison.