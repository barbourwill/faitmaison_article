key: terms
title: Terms & Conditions
language: en


##1 Acceptance &amp; Use Of FaitMaison.co


Your  access  to  and  use  of  FaitMaison.co is  subject exclusively to these Terms and Conditions. You will not use the Website for any purpose that is unlawful or prohibited by these Terms and Conditions. By using  the  Website  you  are  fully  accepting  the  terms,  conditions  and disclaimers contained in this notice. If you do not accept these Terms and Conditions you must immediately stop using the Website.


##2 Payments

FaitMaison.co (Home Cooked Innovations Limited) is a distributor for the Leetchi Corp. S.A. company, a 'société anonyme' under the law of Luxembourg with a share capital of 500,000 Euros, and a registered office located at 26-28, rives de Clausen, L-2165 Luxembourg, listed under the number B173459 in the Luxembourg trade register (hereinafter referred to as “Leetchi”).
            
Leetchi provides services for the issuing, use, and management of e-money. These services are offered on the Website as a means of payment for FaitMaison meals. No other method of payment is available on the Website.</p>
            
Before using FaitMaison.co, we would encourage you to familiarize yourself with the terms and conditions for use of MANGOPAY e-money drawn up by Leetchi, and available in an Appendix <a href="http://www.mangopay.com/wp-content/blogs.dir/10/files/2013/06/EN-Terms-and-conditions-of-Mangopay-September-2013.pdf">(the â€œTerms and Conditions of MangoPayâ€)</a>


##3 Advice

The contents of FaitMaison.co website do not constitute advice and should not be relied upon in making or refraining from making, any decision.


##4 Change of Use

FaitMaison.co reserves the right to:<br /> 4.1 change or remove (temporarily or permanently) the Website or any part of it without notice and you confirm that FaitMaison.co shall not be liable to you for any such change or removal and.<br /> 4.2 change these Terms and Conditions at any time, and your continued use of the Website following any changes shall be deemed to be your acceptance of such change.


##5 Links to Third Party Websites

FaitMaison.co Website may include links to third party websites that are controlled and maintained by others. Any link to other websites is not an endorsement of such websites and you acknowledge and agree that we are not responsible for the content or availability of any such sites.


##6 Copyright 

6.1 All  copyright,  trade  marks  and  all  other  intellectual  property  rights  in  the Website and its content (including without limitation the Website design, text, graphics and all software and source codes connected with the Website) are owned by or   licensed to FaitMaison.co or otherwise used by FaitMaison.co as permitted by law.<br /> 6.2 In accessing the Website you agree that you will access the content solely for your personal, non-commercial use. None of the content may be downloaded, copied, reproduced, transmitted, stored, sold or distributed without the prior written consent of the copyright holder. This excludes the downloading, copying and/or printing of pages of the Website for personal, non-commercial home use only.


##7 Disclaimers and Limitation of Liability 

7.1 The Website is provided on an AS IS and AS AVAILABLE basis without any representation or endorsement made and without warranty of any kind whether express or implied, including but not limited to the implied warranties of satisfactory quality, fitness for a particular purpose, non-infringement, compatibility, security and accuracy.<br /> 7.2 To the extent permitted by law, FaitMaison.co will not be liable for any indirect or consequential loss or damage whatever (including without limitation loss of business, opportunity, data, profits) arising out of or in connection with the use of the Website.<br /> 7.3 FaitMaison.co makes no warranty that the functionality of the Website will be uninterrupted or error free, that defects will be corrected or that the Website or the server that makes it available are free of viruses or anything else which may be harmful or destructive.<br /> 7.4 Nothing in these Terms and Conditions shall be construed so as to exclude or limit the liability of FaitMaison.co for death or personal injury as a result of the negligence of FaitMaison.co or that of its employees or agents.


##8 Indemnity

You agree to indemnify and hold FaitMaison.co (Home Cooked Innovations Limited) and its employees and agents harmless from and against all liabilities, legal fees, damages, losses, costs and other expenses in relation to any claims or actions brought against FaitMaison.co arising out of any breach by you of these Terms and Conditions or other liabilities arising out of your use of this Website.


##9 Severance

If any of these Terms and Conditions should be determined to be invalid, illegal or unenforceable for any reason by any court of competent jurisdiction then such Term or Condition shall be severed and the remaining Terms and Conditions shall survive and remain in full force and effect and continue to be binding and enforceable.


##10 Governing Law

These Terms and Conditions shall be governed by and construed in accordance with the law of the United Kingdom and you hereby submit to the exclusive jurisdiction of the United Kingdom courts.


##Further information

For any further information please email <a href='mailto:terms@faitmaison.co'>terms@faitmaison.co</a>

