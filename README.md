# FaitMaison.co Static article Repository

This will house information pages that appear on www.faitmaison.co/article.

Such as the *terms and conditions* , *privacy_policy* etc

Each entry should be realised as a separate .md (Markdown) file.

The beginning of each file can include some metadata. e.g.

	key: terms
	title: Terms and conditions
	language: en
	